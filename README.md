Xen CI
======

This repo contains [terraform.io](https://terraform.io) configs for the
Xen Project GitLab CI infrastructure.

Usage
-----

#. Install `terraform` for your distro on your machine.
#. Terraform state is stored using the [https://www.terraform.io/docs/backends/types/swift.html](swift backend).  To configure this create `backend.conf` for use with with the following fields:
  - user_name
  - tenant_id
  - password
#. Create `terraform.tfvars` with the following fields:
  - user_name
  - tenant_id
  - api_key
  - password
  - gitlab_token
#. Get shared SSH provision key at `key/id_provision_key`. (TODO: Cloud Files)
#. Run `terraform init -backend-config=backend.conf`
#. Run `terraform plan` to see the changes.
#. Run `terraform apply` to make the changes.


One Time Action
---------------

#. Generate SSH key at `key/id_provision_key`. e.g.
```
ssh-keygen -t rsa -b 4096 -f key/id_provision_key
```
