provider "openstack" {
  user_name = "${var.user_name}"
  tenant_id = "${var.tenant_id}"
  password  = "${var.password}"
  auth_url  = "https://identity.api.rackspacecloud.com/v2.0/"
  region    = "${var.region}"
}

resource "random_pet" "name" {
  separator = "1"
  length    = "1"

  keepers = {
    public_key = "${file("key/id_provision_key.pub")}"
  }
}

resource "openstack_compute_keypair_v2" "provision_key" {
  name       = "provision-key-${random_pet.name.id}"
  public_key = "${random_pet.name.keepers.public_key}"
  region     = "${var.region}"
}

data "template_file" "runners_config" {
  template = "${file("templates/config.toml.tpl")}"

  vars {
    runners_concurrent = "${var.runners_concurrent}"
  }
}

resource "openstack_compute_instance_v2" "gitlab-docker-machine" {
  count     = "1"
  name      = "gitlab-docker-machine-${random_pet.name.id}"
  region    = "${var.region}"
  image_id  = "${var.image_id}"
  flavor_id = "${var.docker_machine_instance_flavor}"
  key_pair  = "${openstack_compute_keypair_v2.provision_key.name}"

  network {
    uuid = "00000000-0000-0000-0000-000000000000"
    name = "public"
  }

  network {
    uuid = "11111111-1111-1111-1111-111111111111"
    name = "private"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      private_key = "${file("key/id_provision_key")}"
    }

    inline = [
      "mkdir -p /etc/gitlab-runner/",
    ]
  }

  provisioner "file" {
    connection {
      type        = "ssh"
      private_key = "${file("key/id_provision_key")}"
    }

    content     = "${data.template_file.runners_config.rendered}"
    destination = "/etc/gitlab-runner/config.toml"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      private_key = "${file("key/id_provision_key")}"
    }

    script = "scripts/setup-bastion-machine.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      private_key = "${file("key/id_provision_key")}"
    }

    inline = [
      "gitlab-runner register --non-interactive -r ${var.gitlab_token} --locked --url ${var.gitlab_url} --executor docker+machine --run-untagged --tag-list x86,x86_64,x86_32,docker-machine --docker-image ${var.gitlab_docker_image} --request-concurrency ${var.runners_concurrent} --limit ${var.runners_limit} --machine-idle-time ${var.runners_idle_time} --machine-idle-nodes ${var.runners_idle_count}  --machine-machine-driver rackspace --machine-machine-name '%s' --machine-machine-options rackspace-username=${var.user_name} --machine-machine-options rackspace-region=${var.region} --machine-machine-options rackspace-api-key=${var.api_key} --machine-machine-options rackspace-image-id=${var.image_id} --machine-machine-options rackspace-flavor-id=${var.runners_instance_flavor} --machine-machine-options rackspace-docker-install=true",
    ]
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      private_key = "${file("key/id_provision_key")}"
    }

    when = "destroy"

    inline = [
      "gitlab-runner unregister --all-runners || :",
      "systemctl stop gitlab-runner",
      "l=`docker-machine ls -q`; if [ -n \"$l\" ]; then  docker-machine rm -y $l || :; fi",
    ]
  }
}

output "gitlab-docker-machine-ip" {
  value = "${openstack_compute_instance_v2.gitlab-docker-machine.access_ip_v4}"
}
