terraform {
  backend "swift" {
    auth_url          = "https://identity.api.rackspacecloud.com/v2.0/"
    region_name       = "IAD"
    container         = "xen-gitlab-runner"
    archive_container = "xen-gitlab-runner-archive"
  }
}
